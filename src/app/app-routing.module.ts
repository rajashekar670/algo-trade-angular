import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BreakoutStrategyFormComponent } from './components/forms/breakout-strategy-form/breakout-strategy-form.component';
import { LoginComponent } from './components/login/login.component';
import { StrategyDetailsComponent } from './components/pages/strategy-details/strategy-details.component';
import { StrategyPageComponent } from './components/pages/strategy-page/strategy-page.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'breakout-strategy',
    component: BreakoutStrategyFormComponent,
  },
  {
    path: 'strategies',
    component: StrategyPageComponent,
  },
  {
    path: 'strategies/:id',
    component: StrategyDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

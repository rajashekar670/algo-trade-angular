import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderModule } from './components/header/header.module';
import { LoginComponent } from './components/login/login.component';
import { LoginModule } from './components/login/login.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BreakoutStrategyFormComponent } from './components/forms/breakout-strategy-form/breakout-strategy-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TextInputComponent } from './components/forms/controls/text-input/text-input.component';
import { FieldErrorComponent } from './components/forms/field-error/field-error.component';
import { SelectInputModule } from './components/forms/controls/select-input/select-input.module';
import { BreakoutStrategyFormModule } from './components/forms/breakout-strategy-form/breakout-strategy-form.module';
import { FieldErrorModule } from './components/forms/field-error/field-error.module';
import { TextInputModule } from './components/forms/controls/text-input/text-input.module';
import {HttpClientModule} from '@angular/common/http';
import { StrategyPageComponent } from './components/pages/strategy-page/strategy-page.component';
import { StrategyDetailsComponent } from './components/pages/strategy-details/strategy-details.component'
import { StrategyModule } from './components/strategy/strategy.module';

@NgModule({
  declarations: [
    AppComponent,
    StrategyPageComponent,
    StrategyDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    HeaderModule,
    LoginModule,
    NgbModule,
    ReactiveFormsModule,
    BreakoutStrategyFormModule,
    TextInputModule,
    FieldErrorModule,
    SelectInputModule,
    StrategyModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
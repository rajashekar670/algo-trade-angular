import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectInputComponent } from './select-input.component';
import { FieldErrorComponent } from '../../field-error/field-error.component';
import { FieldErrorModule } from '../../field-error/field-error.module';

@NgModule({
  declarations: [SelectInputComponent],
  imports: [CommonModule, FieldErrorModule],
  exports: [SelectInputComponent],
})
export class SelectInputModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextInputComponent } from './text-input.component';
import { FieldErrorModule } from '../../field-error/field-error.module';

@NgModule({
  declarations: [TextInputComponent],
  imports: [CommonModule,FieldErrorModule],
  exports: [TextInputComponent],
})
export class TextInputModule {}

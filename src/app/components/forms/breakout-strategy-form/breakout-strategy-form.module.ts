import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreakoutStrategyFormComponent } from './breakout-strategy-form.component';
import { TextInputModule } from '../controls/text-input/text-input.module';
import { SelectInputModule } from '../controls/select-input/select-input.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [BreakoutStrategyFormComponent],
  imports: [CommonModule, TextInputModule, SelectInputModule,ReactiveFormsModule],
  exports: [BreakoutStrategyFormComponent]
})
export class BreakoutStrategyFormModule {}

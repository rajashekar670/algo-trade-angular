import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakoutStrategyFormComponent } from './breakout-strategy-form.component';

describe('BreakoutStrategyFormComponent', () => {
  let component: BreakoutStrategyFormComponent;
  let fixture: ComponentFixture<BreakoutStrategyFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BreakoutStrategyFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakoutStrategyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

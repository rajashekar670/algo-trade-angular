import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StrategyService } from '../../services/strategy.service';

@Component({
  selector: 'app-breakout-strategy-form',
  templateUrl: './breakout-strategy-form.component.html',
  styleUrls: ['./breakout-strategy-form.component.css'],
})
export class BreakoutStrategyFormComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private strategyService: StrategyService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      (params) => (this.strategyId = params['id'])
    );
    if (this.strategyId) {
      this.strategyService.getStrategy(this.strategyId).subscribe((data) => {
        console.log(data);
        this.form = this.fb.group(data);
      });
    }
  }

  strategyId: string = '';

  segmentValues = [
    { key: 'Equity', value: 'EQUITY' },
    { key: 'Options', value: 'OPTIONS' },
  ];

  strategyValues = [{ key: 'Breakout', value: 'BREAKOUT' }];

  statusValues = [
    { key: 'Open', value: 'OPEN' },
    { key: 'Completed', value: 'COMPLETED' },
  ];

  strategyTypeValues = [
    { key: 'NA', value: 'NA' },
    { key: 'Buy', value: 'BUY' },
    { key: 'Sell', value: 'SELL' },
  ];

  expiryValues = [
    { key: 'NA', value: 'NA' },
    { key: 'Weekly', value: 'WEEKLY' },
    { key: 'Monthly', value: 'MONTHLY' },
  ];

  clientValues = [{ key: 'GG4063-Zerodha', value: 'GG4063' }];

  form = this.fb.group({
    userName: ['rajashekar670@gmail.com', [Validators.required]],

    id: ['', [Validators.required, Validators.minLength(6)]],
    client: ['GG4063', [Validators.required]],
    symbol: ['BANKNIFTY', [Validators.required]],
    segment: ['OPTIONS', [Validators.required]],
    strategy: ['BREAKOUT', [Validators.required]],
    status: ['OPEN', [Validators.required]],
    frequency: [1, [Validators.required, Validators.min(1)]],
    label: [''],
    pl: [0, [Validators.required, Validators.min(0)]],

    breakoutHigh: [0, [Validators.required, Validators.min(0)]],
    breakoutLow: [0, [Validators.required, Validators.min(0)]],
    expiry: ['WEEKLY', [Validators.required]],
    expiryDate: ['', [Validators.required]],
    strategyType: ['BUY', [Validators.required]],
    startTime: ['', [Validators.required]],
    endTime: ['', [Validators.required]],
    quantity: [50, [Validators.required, Validators.min(1)]],
    timeFrame: [5, [Validators.required, Validators.min(1)]],
    noOfEntries: [2, [Validators.required, Validators.min(1)]],
    noOfSlHit: [0, [Validators.required, Validators.min(0)]],
    additionalATMPoints: ['', [Validators.required, Validators.min(0)]],
  });

  log() {
    console.log(JSON.stringify(this.form.value));
    alert('success');
  }

  submit() {
    console.log('submitting the form');
    this.strategyService
      .createBreakoutStrategy(this.form.value)
      .subscribe((hero) => this.log());
  }
}

import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { BreakoutStrategyFormModule } from '../forms/breakout-strategy-form/breakout-strategy-form.module';
import { PagedStrategy, Strategy } from '../types/types';

@Injectable({
  providedIn: 'root',
})
export class StrategyService {
  constructor(private http: HttpClient) {}

  START_STRATEGY_URL = '/api/strategies/strategy/breakout';
  ALL_STRATEGY_URL = '/api/strategies';

  createBreakoutStrategy(form: any): Observable<unknown> {
    return this.http
      .post<any>('/api/strategies/strategy/breakout', form, {
        observe: 'response',
      })
      .pipe(catchError(this.handleError));
  }

  getStrategies(status: string): Observable<PagedStrategy> {
    const options = status
      ? { params: new HttpParams().set('status', status) }
      : {};
    return this.http
      .get<any>('/api/strategies', options)
      .pipe(catchError(this.handleError));
  }

  startStrategy(id: string): Observable<any> {
    return this.http
      .get<any>('/api/strategies/' + id + '/actions/start')
      .pipe(catchError(this.handleError));
  }

  getStrategy(id: string): Observable<any> {
    return this.http
      .get<any>('/api/strategies/' + id)
      .pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    alert(JSON.stringify(error.error));
    return throwError(() => new Error('Error occured'));
  }
}

export interface Strategy {
  id: string;
  client: string;
  symbol: string;
  segment: string;
  strategy: string;
  status: string;
  frequency: number;
  label: string;
  pl: number;
  entries: [StrategyEntry];
}

export interface StrategyEntry {
  id: string;
  side: string;
  quantity: number;
  entry: number;
  exit: number;
  status: string;
  ltp: number;
  orderId: string;
  segment: string;
  expiry: string;
  optionStrikePrice: string;
  expiryDate: string;
  optionType: string;
  createdTime: string;
  modifiedTime: string;
}

export interface BreakoutStrategy extends Strategy {
  breakoutHigh: number;
  breakoutLow: number;
  expiry: string;
  expiryDate: string;
  strategyType: string;
  startTime: string;
  endTime: string;
  quantity: number;
  timeFrame: number;
  noOfEntries: number;
  noOfSLHit: number;
  additionalATMPoints: number;
}

export interface PagedStrategy {
    currentPage:number,
    totalPages:number, 
    totalItems:number,
    items:[Strategy]
}

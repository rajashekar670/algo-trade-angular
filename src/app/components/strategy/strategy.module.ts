import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StrategyComponent } from './strategy.component';
import { AppRoutingModule } from 'src/app/app-routing.module';



@NgModule({
  declarations: [
    StrategyComponent
  ],
  imports: [
    CommonModule, AppRoutingModule
  ],exports:[
    StrategyComponent
  ]
})
export class StrategyModule { }

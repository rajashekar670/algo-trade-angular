import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { StrategyService } from '../services/strategy.service';
import { Strategy } from '../types/types';

@Component({
  selector: '.strategy-row',
  templateUrl: './strategy.component.html',
  styleUrls: ['./strategy.component.css'],
})
export class StrategyComponent implements OnInit, OnDestroy {
  constructor(private strategyService: StrategyService) {}
  @Input() strategy: Strategy = {} as Strategy;

  ngOnDestroy(): void {
    this.interval$.unsubscribe();
  }

  interval$!: Subscription;

  ngOnInit(): void {
    this.interval$ = interval(2000).subscribe(() => {
      this.update(this.strategy.id);
    });
  }

  startStrategy(id: string) {
    console.log('Starting the strategy ' + id);
    this.strategyService.startStrategy(id).subscribe();
  }

  update(id: string) {
    if (this.strategy.status === 'Open' || this.strategy.status === 'Running') {
      this.strategyService
        .getStrategy(id)
        .subscribe((data) => (this.strategy = data));
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StrategyModule } from '../../strategy/strategy.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule, StrategyModule
  ]
})
export class StrategyPageModule { }

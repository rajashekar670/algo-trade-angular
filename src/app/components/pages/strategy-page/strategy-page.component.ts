import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { createIncrementalCompilerHost } from 'typescript';
import { StrategyService } from '../../services/strategy.service';
import { PagedStrategy } from '../../types/types';

@Component({
  selector: 'app-strategy-page',
  templateUrl: './strategy-page.component.html',
  styleUrls: ['./strategy-page.component.css'],
})
export class StrategyPageComponent implements OnInit, OnDestroy {
  constructor(private strategyService: StrategyService) {}

  ngOnDestroy(): void {
    this.interval$ && this.interval$.unsubscribe();
  }

  strategies: PagedStrategy = {} as PagedStrategy;

  statusValues = [
    { key: 'All', value: '' },
    { key: 'Open', value: 'OPEN' },
    { key: 'Completed', value: 'COMPLETED' },
    { key: 'Running', value: 'RUNNING' },
  ];

  status:string = ''

  onChange(event:any) {
    this.status = event.target.value;
    this.ngOnInit();
  }

  interval$!: Subscription;

  ngOnInit(): void {
    console.log('Fetching all the strategies');

    this.strategyService
      .getStrategies(this.status)
      .subscribe((data) => (this.strategies = data));
  }

  startStrategy(id: string) {
    console.log('Starting the strategy ' + id);
    this.strategyService.startStrategy(id).subscribe();
  }

  updateData() {
    console.log('updating the open strategy');
    // this.strategyService.getStrategies().subscribe(data=> this.strategies = data);
  }
}

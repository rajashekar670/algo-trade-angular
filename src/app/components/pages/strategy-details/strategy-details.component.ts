import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { StrategyService } from '../../services/strategy.service';

@Component({
  selector: 'app-strategy-details',
  templateUrl: './strategy-details.component.html',
  styleUrls: ['./strategy-details.component.css'],
})
export class StrategyDetailsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private strategyService: StrategyService
  ) {}

  id: string = '';
  strategy: any;
  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
    });
    console.log('Fetching strategy data for ' + this.id);
    this.strategyService
      .getStrategy(this.id)
      .subscribe((data) => (this.strategy = data));
    console.log(this.id);
  }
}
